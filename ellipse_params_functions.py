from glob import glob
import os
import pandas as pd


def append_new_ellipses(data_frame, path_ellipses):
    """
    searches for all the files under path_ellipses named "ellipses_params.xlsx" using glob
    it reads the excel files using pandas and adds them 3 columns(name, number, unique color)
    every ellipse it gets gets appended to the already existing data_frame of ellipses
    """
    new_paths = [y for x in os.walk(path_ellipses) for y in glob(os.path.join(x[0], 'ellipse_params.xlsx'))]
    for file_path in new_paths:
        file_values = pd.read_excel(file_path)
        for index, ellipse in file_values.iterrows():
            ellipse['Name'] = file_path.split('\\')[-2]
            ellipse['Number'] = index
            ellipse['Unique Color'] = 'nan'
            ellipse['Visibility'] = 1
            data_frame.append(ellipse)  # adds ellipse tuple containing x, y, theta, major, minor

    return data_frame


def save_data_frame(data_frame, path_file):
    """
    saves the data_frame using pandas
    """
    df = pd.DataFrame(data_frame)
    df.to_excel(path_file, index=False)


def make_ellipse_params_file(path_ellipses):
    """
    The function gets the path to the folder which has the ikuns (let's call it the main folder). It goes over all the
    folders inside the main folder, takes the value of the excel file inside each folder, which contains the parameters
    of the ellipses, and creates an excel file called "ellipse_params.xlsx" inside the main folder, which contains all
    the ellipses parameters from all the ikuns.
    :param path_ellipses: String, path to the main folder which contains the ikuns.
    :return: String, path to the created excel file with all the ellipses' values.
    """
    data_frame = append_new_ellipses([], path_ellipses)
    path_new_file = os.path.join(path_ellipses + '/ellipses_map.xlsx')
    save_data_frame(data_frame, path_new_file)

    return path_new_file


def add_ellipse_params_file(path_ellipses, path_old_file):
    """
    The function gets the path to the folder which has the ikuns (let's call it the main folder). It goes over all the
    folders inside the main folder, takes the value of the excel file inside each folder, which contains the parameters
    of the ellipses, and creates an excel file called "ellipse_params.xlsx" inside the main folder, which contains all
    the ellipses parameters from all the ikuns.

    adding new ikuns doesn't saves itself. in order to save the added ikuns push the save button

    :param path_ellipses: String, path to the main folder which contains the new ikuns.
    :param path_old_file: String, path to the folder which contains the existing ellipses' values.
    :return: String, path to the excel file with all the ellipses' values.
    """
    existing_file = pd.read_excel(path_old_file)
    data_frame = []
    for _, ellipse in existing_file.iterrows():
        data_frame.append(ellipse)

    data_frame = append_new_ellipses(data_frame, path_ellipses)
    save_data_frame(data_frame, path_old_file)

    return path_old_file


def get_ellipse_values(ellipse_params):
    """
    this function returns a single ellipse parameters for the main code to be cleaner
    it calls the values under certain keys in the panda's format data_frame line
    and returns them to the main code to be plotted to the map
    """
    name = ellipse_params['Name']
    number = ellipse_params['Number']
    x = ellipse_params['X']
    y = ellipse_params['Y']
    width_radius = ellipse_params['Width Radius']
    height_radius = ellipse_params['Height Radius']
    theta = ellipse_params['Theta']
    unique_color = ellipse_params['Unique Color']
    visibility = ellipse_params['Visibility']
    return name, number, x, y, width_radius, height_radius, theta, unique_color, visibility
