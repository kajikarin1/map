class MyLegend:
    num_ellipses_in_legend = 32

    def __init__(self, ax, ellipses):
        self.ax = ax
        self.ellipses_in_legend = {}

        self.start_pos_legend = 0
        self.end_pos_legend = min(self.num_ellipses_in_legend, len(ellipses))

        self.add_label_ellipses_in_legend(ellipses[self.start_pos_legend:self.end_pos_legend])
        self.map_legend = self.set_legend(ellipses[self.start_pos_legend:self.end_pos_legend])

    def set_legend(self, ellipses):
        """
        standard map legend creation function(it gets all the plots from ax and gets their label)
        """
        map_legend = self.ax.legend(bbox_to_anchor=(1, 1), loc='upper left', fancybox=True, shadow=True)
        map_legend.get_frame().set_alpha(0.75)

        for legend_ellipse_box, ellipse in zip(map_legend.get_patches(), ellipses):
            legend_ellipse_box.set_picker(True)
            self.ellipses_in_legend[ellipse] = legend_ellipse_box

        return map_legend

    def update_legend(self, ellipses):
        """
        a function to redraw the legend whenever it had been changed(ellipse change color, ellipse got deleted)
        """
        self.map_legend.remove()
        self.ellipses_in_legend = {}
        self.add_label_ellipses_in_legend(ellipses[self.start_pos_legend:self.end_pos_legend])
        self.map_legend = self.set_legend(ellipses[self.start_pos_legend:self.end_pos_legend])

    def change_ellipse_color_in_legend(self, picked_ellipse):
        """
        whenever an ellipse's color is being changed it calls this function to update the information in the legend
        """
        try:
            self.ellipses_in_legend[picked_ellipse].set_edgecolor(picked_ellipse.color)
        except KeyError:
            pass

    @staticmethod
    def add_label_ellipses_in_legend(ellipses):
        for ellipse in ellipses:
            ellipse.ellipsePlot.set_label(ellipse.name)

    @staticmethod
    def remove_label_ellipses_in_legend(ellipses):
        for ellipse in ellipses:
            ellipse.ellipsePlot.set_label(None)
