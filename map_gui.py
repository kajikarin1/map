import PySimpleGUI as Sg
from ikun_ellipse_map.ellipse_params_functions import make_ellipse_params_file, add_ellipse_params_file


def main_tab():
    """
    main window for the gui
    everything you see when you look at the gui is contained here
    """
    Sg.theme('Dark Purple 4')
    # noinspection PyTypeChecker
    main_layout = [[Sg.Text('Choose Map Layout')],
                   [Sg.Radio('Existing Map', "RADIO1", key='existing map', default=True, enable_events=True),
                    Sg.Radio('New Map', "RADIO1", key='new map', enable_events=True),
                    Sg.Radio('Existing Map + New Ikuns', "RADIO1", key='existing map + new ikuns', enable_events=True)],

                   [Sg.Column(
                       [[Sg.Text('New Ikuns Folders', size=(15, 1), key='new ikuns text', enable_events=True),
                         Sg.InputText(key='new ikuns input text', enable_events=True),
                         Sg.FolderBrowse(key='new ikun folder', enable_events=True)]],
                       key='new ikuns')],

                   [Sg.Column(
                       [[Sg.Text('Ikun Params File', size=(15, 1), key='old ikuns text', enable_events=True),
                         Sg.InputText(key='old ikuns input text', enable_events=True),
                         Sg.FileBrowse(key='old ikun file', file_types=(('xlsx Files', '*.xlsx'),),
                         enable_events=True)]],
                       key='old ikuns')],

                   [Sg.Column(
                       [[Sg.Text('Geometry File', size=(15, 1), key='geom file text', enable_events=True),
                         Sg.InputText(key='geom file input text', enable_events=True),
                         Sg.FileBrowse(key='geom file', file_types=(('csv Files', '*csv'),),
                                       enable_events=True)]],
                       key='geom')],

                   [Sg.Button('Run', key='Run'), Sg.Cancel('Cancel')]]

    settings_layout = [[Sg.Text('Ellipses Colors:', size=(15, 1))],
                       [Sg.Radio('Dark Orange', "RADIO2", key='dark orange', default=True),
                        Sg.Radio('Colored', "RADIO2", key='colored')],

                       [Sg.Text('Plot Tiff:', size=(15, 1))],
                       [Sg.Radio('Yes', "RADIO3", key='yes tiff'),
                        Sg.Radio('No', "RADIO3", key='no tiff', default=True)],

                       [Sg.Text('Use Point Object:', size=(15, 1))],
                       [Sg.Radio('Yes', "RADIO4", key='yes point object'),
                        Sg.Radio('No', "RADIO4", key='no point object', default=True)],

                       [Sg.Text('Plot Points Name:', size=(15, 1))],
                       [Sg.Radio('Yes', "RADIO5", key='yes points name'),
                        Sg.Radio('No', "RADIO5", key='no points name', default=True)]]

    layout = [[Sg.TabGroup([[Sg.Tab('Ikun Map', main_layout, key='Main Tab'),
                             Sg.Tab('Settings', settings_layout, key='Settings')]])]]

    window_main = Sg.Window('Ikun Ellipse Map', layout, return_keyboard_events=True, finalize=True)

    window_main.Element('new ikuns').Update(visible=False)
    window_main.Element('old ikuns').Update(visible=True)

    return window_main


def main_loop(window_main):
    """
    this function waits for every change in the gui and preventing it form shutting down until the run, exit and cancel
     buttons are being pressed
    """
    window_main_active = True
    values = []

    while window_main_active:  # Event Loop

        event, values = window_main.read()

        if event in (None, 'Run', 'Exit', 'Cancel'):
            window_main_active = False

        if event == 'existing map':
            window_main.Element('new ikuns').Update(visible=False)
            window_main.Element('old ikuns').Update(visible=True)

        if event == 'new map':
            window_main.Element('new ikuns').Update(visible=True)
            window_main.Element('old ikuns').Update(visible=False)

        if event == 'existing map + new ikuns':
            window_main.Element('new ikuns').Update(visible=True)
            window_main.Element('old ikuns').Update(visible=True)

    window_main.close()

    return values


def get_map_path(values):
    """
    map_path is excel file containing the ellipses x, y, width, height, theta, name, number, unique color
    in case its an existing map it just returns the map_path
    in case its a new map it searches for every ellipse_params file in the given folder combines them and returns the
     map_path
    in case its an existing map with new ikuns it combines the old ellipses params with the new found ellipses params
     and returns the map_path
    """
    map_path = ''

    if values['existing map']:
        map_path = values['old ikun file']

    elif values['new map']:
        new_ikuns_path = values['new ikun folder']
        map_path = make_ellipse_params_file(new_ikuns_path)

    elif values['existing map with new ikuns']:
        map_path = values['old ikun file']
        new_ikuns_path = values['new ikun folder']
        map_path = add_ellipse_params_file(new_ikuns_path, map_path)

    return map_path
