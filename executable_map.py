from matplotlib import pyplot as plt
from matplotlib.widgets import Button
import math

import pandas as pd
import PySimpleGUI as Sg
from pathlib import Path

from ikun_ellipse_map.LegendObject import MyLegend
from ikun_ellipse_map.map_gui import main_loop, main_tab, get_map_path
from ikun_ellipse_map.ellipse_params_functions import get_ellipse_values
from ikun_ellipse_map.EllipseObject import MyEllipse
from ikun_ellipse_map.PointObject import MyPoint
from ikun_ellipse_map.tiff_functions import produce_tiff

if_measure_clicked = False


def save_ellipses(event):
    """
    button event that saves the ellipse parameters in an xlsx file
    it writes over the existing file so the changes the user did would be saved
    """
    df = pd.DataFrame([save_ellipse.get_plot().get_gid() for save_ellipse in ellipses])
    df.to_excel(map_path, index=False)
    Sg.popup_notify('Map Saved')
    return map_path


def measure_distance(event):
    """
    button event for measuring distance between 2 clicked points
    it disables the ellipse change_color function before starting
    and activates it after finishing
    the measured distance value pops up at the right down side or the screen
    """
    global if_measure_clicked
    if_measure_clicked = True

    measure_points = plt.ginput(2)
    x_value = (measure_points[1][0] - measure_points[0][0])**2
    y_value = (measure_points[1][1] - measure_points[0][1])**2
    distance = math. sqrt(x_value + y_value)
    Sg.popup_notify(distance)

    if_measure_clicked = False
    return if_measure_clicked


def on_plot_hover_line(event):
    """
    hover event for the ellipse ID
    it writes the ID on top of the map window
    at a format of: Name, X, Y, Width Radius, Height Radius, Theta, Color
    """
    for on_hover_points in ax.patches:
        if on_hover_points.contains(event)[0]:
            gid_ellipse = on_hover_points.get_gid()
            gid_ellipse_name = gid_ellipse['Name']
            gid_ellipse_number = gid_ellipse['Number']
            gid_ellipse_x = round(gid_ellipse['X'], 2)
            gid_ellipse_y = round(gid_ellipse['Y'], 2)
            gid_ellipse_width = round(gid_ellipse['Width Radius'], 2)
            gid_ellipse_height = round(gid_ellipse['Height Radius'], 2)
            gid_ellipse_theta = round(gid_ellipse['Theta'], 2)
            gid_ellipse_unique_color = gid_ellipse['Unique Color']

            gid_info = f"Name_Number: {gid_ellipse_name}_{gid_ellipse_number}, Unique Color: {gid_ellipse_unique_color}"
            gid_params = f"X: {gid_ellipse_x}, Y: {gid_ellipse_y}, Theta: {gid_ellipse_theta}, " \
                         f"Width Radius: {gid_ellipse_width}, Height Radius: {gid_ellipse_height}"

            ax.set_title(f"{gid_info} \n {gid_params}")
            plt.draw()  # redraws figure, do for any update


def on_plot_click_line(event):
    """
    click event for changing ellipse color
    the default ellipse color is orange or scaled color(depends on what had been chosen in gui)
    if left clicked the ellipse changes to red
    if wheel clicked the ellipse and her mirror ellipse changes to blue
    if the ellipse is clicked again when its red or blue it returns to orange
    if right clicked the ellipse gets deleted from the map
    when an ellipse is blue or red it cannot be deleted
    """
    if not if_measure_clicked:
        clicked_ellipses_name = []
        clicked_ellipses_gid = []
        for on_click_points in ax.patches:
            if on_click_points.contains(event)[0]:
                clicked_ellipses_name.append(on_click_points.get_gid()['Name'])  # list of clicked ellipses name
                clicked_ellipses_gid.append(on_click_points.get_gid())  # list of clicked ellipses gid
        for search_ellipse in ellipses:
            if search_ellipse.get_plot().get_gid()['Name'] in clicked_ellipses_name:
                if search_ellipse.get_plot().get_gid() in clicked_ellipses_gid:
                    # left click
                    if event.button == 1:
                        search_ellipse.change_color(event.button)
                        if search_ellipse.get_plot().get_label() is not None:
                            ui_legend.change_ellipse_color_in_legend(search_ellipse)
                        break
                    # right click
                    if event.button == 3 and search_ellipse.color not in ['magenta', 'blue']:
                        line = search_ellipse.get_plot()
                        line_label = line.get_label()
                        ellipses.remove(search_ellipse)
                        line.remove()
                        if line_label is not None:
                            ui_legend.update_legend(ellipses)
                        break
                # wheel click
                if event.button == 2:
                    for inside_ellipse in ellipses:
                        inside_ellipse_name = inside_ellipse.get_plot().get_gid()['Name']
                        if inside_ellipse_name == search_ellipse.name:
                            inside_ellipse.change_color(event.button)
                            if inside_ellipse.get_plot().get_label() is not None:
                                ui_legend.change_ellipse_color_in_legend(inside_ellipse)
                    break
        plt.draw()  # redraws figure, do for any update


def on_pick_legend(event):
    """
    when mouse is over the line of an ellipse in the legend and is clicked with left button the ellipse will disappear
     from the map and the ellipse line in the legend will be a bit "ghosted"
    """
    if event.mouseevent.button == 1:
        legend_in_line = event.artist
        for ellipse_in_legend in ui_legend.ellipses_in_legend:
            if ui_legend.ellipses_in_legend[ellipse_in_legend] == legend_in_line:
                original_line = ellipse_in_legend

                original_line.change_visibility()
                visible = original_line.visible
                legend_in_line.set_alpha(visible)

                fig.canvas.draw()
                return ui_legend.ellipses_in_legend


def legend_scroll(event):
    """
    when mouse is over the legend and is scrolled with wheel up or down the legend will go the correct way
    """
    scroll_movement = {"down": 5, "up": -5}
    if ui_legend.map_legend.contains(event):
        if event.button in ('down', 'up'):
            ui_legend.remove_label_ellipses_in_legend(ellipses)

            ui_legend.start_pos_legend += scroll_movement[event.button]
            ui_legend.start_pos_legend = max(0, ui_legend.start_pos_legend)
            ui_legend.end_pos_legend = ui_legend.start_pos_legend + ui_legend.num_ellipses_in_legend
            ui_legend.end_pos_legend = min(ui_legend.end_pos_legend, len(ellipses))

            if ui_legend.end_pos_legend-ui_legend.start_pos_legend <= 5:
                ui_legend.start_pos_legend -= 5

            ui_legend.update_legend(ellipses)
            fig.canvas.draw_idle()
    return ui_legend.map_legend


if __name__ == '__main__':
    fig, ax = plt.subplots()

    window_main = main_tab()
    values = main_loop(window_main)

    geom_file_path = values['geom file']
    points = []
    if Path(geom_file_path).is_file():
        geom_file = pd.read_csv(geom_file_path).values
        if values['yes tiff']:
            produce_tiff(geom_file, ax)
        geom_file = geom_file[:, 1:4]
        if values['yes point object']:
            for sensor in geom_file:  # making sensors object array
                points.append(MyPoint(*sensor, ax))
        else:
            ax.scatter(geom_file[:, 1], geom_file[:, 2], color='purple')
        if values['yes points name']:
            for index, sensor in enumerate(geom_file):
                ax.annotate(sensor[0], (sensor[1], sensor[2]))

    map_path = get_map_path(values)
    ellipses_params = pd.read_excel(map_path)
    ellipses = []

    for index, ellipse_params in ellipses_params.iterrows():
        name, number, x, y, width, height, theta, unique_color, visibility = get_ellipse_values(ellipse_params)
        if values['colored']:  # making ellipse object array with colored ellipses
            colors = MyEllipse.choose_colors(ellipses_params.shape[0])
            ellipses.append(MyEllipse(name, number, x, y, width, height, theta, unique_color, colors[index], visibility, ax))
        else:  # elif values['dark orange']:  # making ellipse object array with darkorange ellipses
            ellipses.append(MyEllipse(name, number, x, y, width, height, theta, unique_color, 'darkorange', visibility, ax))

    ui_legend = MyLegend(ax, ellipses)

    # axes settings
    plt.gca().set_aspect("equal", adjustable="box")  # axis equal
    plt.grid(color='lightgray', linestyle='--')  # shows plot grid
    plt.ticklabel_format(useOffset=False)  # get rid of scientific notation
    plt.ticklabel_format(style='plain')  # get rid of scientific notation

    # graph events
    on_hover_ellipse = fig.canvas.mpl_connect('motion_notify_event', on_plot_hover_line)
    on_click_ellipse = fig.canvas.mpl_connect('button_press_event', on_plot_click_line)
    on_click_legend = fig.canvas.mpl_connect('pick_event', on_pick_legend)
    on_scroll_legend = fig.canvas.mpl_connect('scroll_event', legend_scroll)

    # save button
    save_button_axes = plt.axes([0, 0, 0.1, 0.075])
    save_button = Button(save_button_axes, 'Save Map')
    on_save_clicked = save_button.on_clicked(save_ellipses)

    # measure distance button
    measure_button_axes = plt.axes([0.1, 0, 0.1, 0.075])
    measure_button = Button(measure_button_axes, 'Measure Tool')
    on_measure_clicked = measure_button.on_clicked(measure_distance)

    manager = plt.get_current_fig_manager()
    manager.window.showMaximized()
    plt.show()
