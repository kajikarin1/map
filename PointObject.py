class MyPoint:
    """
    class object for the sensors points
    it contains the Name of the sensor, the X and Y values location
    and a drawing function
    """
    def __init__(self, name, x, y, ax):
        self.name = name
        self.x = x
        self.y = y
        self.color = 'purple'
        self.width = 0.01
        self.pointPlot = None
        self.ax = ax
        self.draw_point()

    def draw_point(self):
        self.pointPlot = self.ax.scatter(self.x, self.y, color=self.color, linewidth=self.width,
                                         gid=(self.name, self.x, self.y))  # rotated ellipse
