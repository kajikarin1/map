from matplotlib.patches import Ellipse
import numpy as np


class MyEllipse:
    """
    class object for the ellipses
    it contains the - Name, X, Y, Rx, Ry, Theta, Color - of the ellipse
    """
    def __init__(self, name, number, x, y, width_radius, height_radius, theta, unique_color, color, visible, ax):
        self.number = number
        self.name = name

        self.x = x
        self.y = y
        self.width_radius = width_radius
        self.height_radius = height_radius
        self.original_theta = theta
        self.theta = theta  # * pi / 180  # converted to radians

        self.color = color
        self.original_color = color
        self.unique_color = unique_color
        if str(self.unique_color) != 'nan':
            self.color = unique_color

        self.visible = visible

        self.LINE_WIDTH_DEFAULT = '0.75'
        self.LINE_WIDTH_UNIQUE = '1.5'
        self.ell = None
        self.ellipsePlot = None

        self.ax = ax
        self.draw_ellipse_matplotlib_patch()

    def draw_ellipse_matplotlib_patch(self):
        if str(self.color) in ['magenta', 'blue']:
            self.ellipsePlot = Ellipse(xy=(self.x, self.y), width=self.width_radius, height=self.height_radius,
                                       angle=self.theta, facecolor='none', edgecolor=self.color,
                                       linewidth=self.LINE_WIDTH_UNIQUE,
                                       gid=({'X': self.x, 'Y': self.y,
                                             'Width Radius': self.width_radius,
                                             'Height Radius': self.height_radius,
                                             'Theta': self.original_theta,
                                             'Name': self.name, 'Number': self.number,
                                             'Unique Color': str(self.color),
                                             'Visibility': self.visible}))
        else:
            self.ellipsePlot = Ellipse(xy=(self.x, self.y), width=self.width_radius, height=self.height_radius,
                                       angle=self.theta, facecolor='none', edgecolor=self.original_color,
                                       linewidth=self.LINE_WIDTH_DEFAULT,
                                       gid=({'X': self.x, 'Y': self.y,
                                             'Width Radius': self.width_radius,
                                             'Height Radius': self.height_radius,
                                             'Theta': self.original_theta,
                                             'Name': self.name, 'Number': self.number,
                                             'Unique Color': str('nan'),
                                             'Visibility': self.visible}))
        self.ellipsePlot.set_alpha(self.visible)
        self.ax.add_patch(self.ellipsePlot)

    """
    these functions changes the ellipse according to legend picking
    """
    def change_visibility(self):
        self.visible = self.ellipsePlot.get_alpha()
        self.visible = 1 if self.visible == 0 else 0
        self.ellipsePlot.set_alpha(self.visible)
        self.set_ellipse_plot_gid()

    def set_ellipse_plot_gid(self):
        self.ellipsePlot.set_gid(gid=({'X': self.x, 'Y': self.y,
                                       'Width Radius': self.width_radius,
                                       'Height Radius': self.height_radius,
                                       'Theta': self.original_theta,
                                       'Name': self.name, 'Number': self.number,
                                       'Unique Color': str('nan'),
                                       'Visibility': self.visible}))

    """
    these functions changes the ellipse according to the requested color from the function 'on_plot_click_line'
    """
    def set_default(self):
        self.color = self.original_color
        self.unique_color = 'nan'
        self.ellipsePlot.set_edgecolor(self.original_color)

    def set_magenta(self):
        self.color = 'magenta'
        self.unique_color = 'magenta'
        self.ellipsePlot.set_edgecolor('magenta')

    def set_blue(self):
        self.color = 'blue'
        self.unique_color = 'blue'
        self.ellipsePlot.set_edgecolor('blue')

    """
    these functions returns the value requested from the function 'on_plot_click_line'
    """
    def get_plot(self):
        return self.ellipsePlot

    def __str__(self):
        """
        ellipse ID function - returns a string of the ellipse values
        """
        return "name: " + str(self.name) + ", x: " + str(self.x) + ", y: " + str(self.y) + "\n" + \
               "major radius: " + str(self.width_radius) + ", minor radius: " + str(self.height_radius) + \
               ", angle: " + str(self.original_theta)

    def change_color(self, mouse_button):
        """
        ellipse change color function
        when left clicked changes the ellipse from orange to red and reverse
        when wheel clicked changes the ellipse and her mirror ellipse from orange to blue and reverse
        if an ellipse is red it cannot be change to blue
        if an ellipse is blue it cannot be changed to red
        you have to return the ellipse back to orange in order to change the color in to another group color
        """
        if mouse_button == 1:  # left click
            if self.color != 'magenta' and self.color != 'blue':
                self.set_magenta()
            elif self.color == 'magenta':
                self.set_default()
        if mouse_button == 2:  # wheel click
            if self.color != 'magenta' and self.color != 'blue':
                self.set_blue()
            elif self.color == 'blue':
                self.set_default()

    @staticmethod
    def choose_colors(file_length):
        """
        this function assigns scaled colors between the colors green and red for the ellipses to be plotted and starts by
         assigning half the color cells with full red color and green is going down from 1 to zero
        then in takes it array and flips in for the full green side which is assigned with red color going up
        the color format is RGBA[Red, Green, Blue, Alpha]
        example with 7 e:
        [0, 1, 0, 1], [0.33, 1, 0, 1], [0.66, 1, 0, 1], [1, 1, 0, 1], [1, 0.66, 0, 1], [1, 0.33, 0, 1], [1, 0, 0, 1]
        """
        local_colors = np.zeros((file_length, 4))
        local_colors[:, 3] = 1

        dx = 2 / file_length

        green = np.zeros(file_length)
        green[:int(len(green) / 2)] = 1
        for i in range(int(len(green) / 2), len(green)):
            green[i] = max(green[i-1] - dx, 0)

        red = green[::-1]

        local_colors[:, 0] = red
        local_colors[:, 1] = green

        return local_colors
